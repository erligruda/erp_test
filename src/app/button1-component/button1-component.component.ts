import { Component, OnInit, HostListener, Input } from '@angular/core';
import { Center1ComponentComponent } from '../center1-component/center1-component.component';

@Component({
  selector: 'app-button1-component',
  templateUrl:'./button1-component.component.html',
  styleUrls: ['./button1-component.component.css']
})

export class Button1ComponentComponent implements OnInit {

  @Input() center1: Center1ComponentComponent;
   
  
  @HostListener ('click')
  
  click() {
    this.center1.toggle();
  }
  constructor() { }

  ngOnInit(): void {
  }

}
