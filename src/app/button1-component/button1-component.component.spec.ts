import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Button1ComponentComponent } from './button1-component.component';

describe('Button1ComponentComponent', () => {
  let component: Button1ComponentComponent;
  let fixture: ComponentFixture<Button1ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Button1ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Button1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
