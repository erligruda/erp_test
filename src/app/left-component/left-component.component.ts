import { Component, OnInit, Input, HostListener} from '@angular/core';
import { Button1ComponentComponent } from '../button1-component/button1-component.component';
import { Button2ComponeneComponent } from '../button2-componene/button2-componene.component';
import { Button3ComponentComponent } from '../button3-component/button3-component.component';
import { CenterComponentComponent } from '../center-component/center-component.component';
import { Center2ComponentComponent } from '../center2-component/center2-component.component';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-left-component',
  templateUrl: './left-component.component.html',
  styleUrls: ['./left-component.component.css']
})
export class LeftComponentComponent implements OnInit {

  @Input() center: CenterComponentComponent;

  @HostListener ('click') 

  click(){ 
    // this.center.toggle();
  }
  

  constructor() { }

  ngOnInit(): void {
  }

}
