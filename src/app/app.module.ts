import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { LeftComponentComponent } from './left-component/left-component.component';
import { Button1ComponentComponent } from './button1-component/button1-component.component';
import { Button2ComponeneComponent } from './button2-componene/button2-componene.component';
import { Button3ComponentComponent } from './button3-component/button3-component.component';
import { CenterComponentComponent } from './center-component/center-component.component';
import { Center1ComponentComponent } from './center1-component/center1-component.component';
import { Center2ComponentComponent } from './center2-component/center2-component.component';
import { Center3ComponentComponent } from './center3-component/center3-component.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    LeftComponentComponent,
    Button1ComponentComponent,
    Button2ComponeneComponent,
    Button3ComponentComponent,
    CenterComponentComponent,
    Center1ComponentComponent,
    Center2ComponentComponent,
    Center3ComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
