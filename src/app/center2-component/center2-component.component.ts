import { Component, OnInit, HostBinding } from '@angular/core';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-center2-component',
  templateUrl: './center2-component.component.html',
  styleUrls: ['./center2-component.component.css']
})
export class Center2ComponentComponent implements OnInit {
  
  @HostBinding('class.is-open')
  isOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
