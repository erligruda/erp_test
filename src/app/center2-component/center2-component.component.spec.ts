import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Center2ComponentComponent } from './center2-component.component';

describe('Center2ComponentComponent', () => {
  let component: Center2ComponentComponent;
  let fixture: ComponentFixture<Center2ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Center2ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Center2ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
