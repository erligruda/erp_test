import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Button2ComponeneComponent } from './button2-componene.component';

describe('Button2ComponeneComponent', () => {
  let component: Button2ComponeneComponent;
  let fixture: ComponentFixture<Button2ComponeneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Button2ComponeneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Button2ComponeneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
