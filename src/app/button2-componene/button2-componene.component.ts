import { Component, OnInit, HostListener, Input } from '@angular/core';
import { Center2ComponentComponent} from '../center2-component/center2-component.component';

@Component({
  selector: 'app-button2-componene',
  templateUrl: './button2-componene.component.html',
  styleUrls: ['./button2-componene.component.css']
})
export class Button2ComponeneComponent implements OnInit {
  @Input() center2: Center2ComponentComponent;

  @HostListener ('click') 

  click(){ 
    this.center2.toggle();
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
