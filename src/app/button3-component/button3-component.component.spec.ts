import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Button3ComponentComponent } from './button3-component.component';

describe('Button3ComponentComponent', () => {
  let component: Button3ComponentComponent;
  let fixture: ComponentFixture<Button3ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Button3ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Button3ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
