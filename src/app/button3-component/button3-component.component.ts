import { Component, OnInit, Input, HostListener, Output } from '@angular/core';
import { Center3ComponentComponent} from '../center3-component/center3-component.component';
import { CenterComponentComponent } from '../center-component/center-component.component';
@Component({
  selector: 'app-button3-component',
  templateUrl: './button3-component.component.html',
  styleUrls: ['./button3-component.component.css']
})
export class Button3ComponentComponent implements OnInit {

  
   @Input() center3: Center3ComponentComponent;

 
 
  @HostListener ('click')
  click() {
    this.center3.toggle();
  }
  // messagetosend: object=function click() {
      // this.center3.toggle();
    //  this.center3.toggle();
  
  
  // click() {
  //   this.center3.toggle();
  // }
  constructor() {
    // console.log(this.center3);
    // console.log(this.messagetosend);
   }

  ngOnInit(): void {
  }

}
