import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {

  @HostBinding('class.is-open')

  isOpen = true;

  // toggle() {
  //   this.isOpen = !this.isOpen;
  // }
  constructor() { }

  ngOnInit(): void {
  }

}
