import { Component, OnInit, HostBinding, Input, Output } from '@angular/core';
import { Button3ComponentComponent } from '../button3-component/button3-component.component';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-center3-component',
  templateUrl: './center3-component.component.html',
  styleUrls: ['./center3-component.component.css']
})
export class Center3ComponentComponent implements OnInit {
  // @Input() receivebtn3msg: object;
    @HostBinding('class.is-open')

  isOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }
  constructor() { 
    // console.log(this.receivebtn3msg);
  }

  ngOnInit(): void {
  }

}
