import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Center3ComponentComponent } from './center3-component.component';

describe('Center3ComponentComponent', () => {
  let component: Center3ComponentComponent;
  let fixture: ComponentFixture<Center3ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Center3ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Center3ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
