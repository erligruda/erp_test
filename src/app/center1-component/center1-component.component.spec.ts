import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Center1ComponentComponent } from './center1-component.component';

describe('Center1ComponentComponent', () => {
  let component: Center1ComponentComponent;
  let fixture: ComponentFixture<Center1ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Center1ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Center1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
