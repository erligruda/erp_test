import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-center1-component',
  templateUrl: './center1-component.component.html',
  styleUrls: ['./center1-component.component.css']
})
export class Center1ComponentComponent implements OnInit {

 @HostBinding('class.is-open')
  isOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }
  
  
  constructor() { }

  ngOnInit(): void {
  }

}
